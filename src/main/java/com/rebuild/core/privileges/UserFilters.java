/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.privileges;

import cn.devezhao.bizz.security.member.Member;
import cn.devezhao.bizz.security.member.Team;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.privileges.bizz.Department;
import com.rebuild.core.privileges.bizz.User;
import com.rebuild.core.privileges.bizz.ZeroEntry;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


@Slf4j
public class UserFilters {

    
    public static boolean isEnableBizzPart(ID user) {
        if (UserHelper.isAdmin(user)) return false;
        return Application.getPrivilegesManager().allow(user, ZeroEntry.EnableBizzPart);
    }

    
    public static boolean allowAccessBizz(ID user, ID bizzId) {
        if (user.equals(bizzId) || UserHelper.isAdmin(user)) return true;
        if (bizzId.getEntityCode() == EntityHelper.Role) return false;

        if (isEnableBizzPart(user)) {
            final User ub = Application.getUserStore().getUser(user);

            if (bizzId.getEntityCode() == EntityHelper.Department) {
                return ub.getOwningDept() != null && bizzId.equals(ub.getOwningDept().getIdentity());
            }

            if (bizzId.getEntityCode() == EntityHelper.Team) {
                for (Team m : ub.getOwningTeams()) {
                    if (bizzId.equals(m.getIdentity())) return true;
                }
                return false;
            }

            if (bizzId.getEntityCode() == EntityHelper.User) {
                return user.equals(bizzId);
            }
        }

        return true;
    }

    
    @SuppressWarnings({"ConstantValue", "SuspiciousMethodCalls"})
    public static Member[] filterMembers32(Member[] members, ID currentUser) {
        if (members == null || members.length == 0) return members;
        if (!isEnableBizzPart(currentUser)) return members;

        final String depth = "D";  

        final User user = Application.getUserStore().getUser(currentUser);
        final Department dept = user.getOwningDept();
        final Set<ID> deptChildren = "D".equals(depth) ? UserHelper.getAllChildren(dept) : Collections.emptySet();

        Set<Member> filteredMembers = new HashSet<>();
        for (Member m : members) {
            if (m instanceof User) {
                
                if (dept.isMember(m.getIdentity())) filteredMembers.add(m);
                
                if ("D".equals(depth)) {
                    for (ID child : deptChildren) {
                        if (Application.getUserStore().getDepartment(child).isMember(m.getIdentity())) {
                            filteredMembers.add(m);
                        }
                    }
                }

            } else if (m instanceof Department) {
                
                if (dept.equals(m)) filteredMembers.add(m);
                
                if ("D".equals(depth) && deptChildren.contains(m.getIdentity())) filteredMembers.add(m);

            } else if (m instanceof Team) {
                
                if (((Team) m).isMember(user.getId())) filteredMembers.add(m);
            }
        }

        return filteredMembers.toArray(new Member[0]);
    }

    
    public static String getEnableBizzPartFilter(int bizzEntityCode, ID user) {
        if (!isEnableBizzPart(user)) return null;

        final User ub = Application.getUserStore().getUser(user);
        Set<ID> in = new HashSet<>();
        String where;

        if (bizzEntityCode == EntityHelper.Team) {
            for (Member m : ub.getOwningTeams()) in.add((ID) m.getIdentity());

            if (in.isEmpty()) {
                where = "1=2";
            } else {
                where = "teamId in ('" + StringUtils.join(in, "','") + "')";
            }

        } else {
            if (bizzEntityCode == EntityHelper.Role) return "1=2";

            in.add((ID) ub.getOwningDept().getIdentity());
            in.addAll(UserHelper.getAllChildren(ub.getOwningDept()));

            
            where = "deptId in ('" + StringUtils.join(in, "','") + "')";
        }

        return where;
    }

    
    public static String getBizzFilter(int bizzEntityCode, ID user) {
        
        if (bizzEntityCode == EntityHelper.User) {
            return String.format("userId <> '%s'", UserService.SYSTEM_USER);
        }
        
        if (bizzEntityCode == EntityHelper.Role && !UserHelper.isAdmin(user)) {
            return "(1=2)";
        }
        return null;
    }
}
