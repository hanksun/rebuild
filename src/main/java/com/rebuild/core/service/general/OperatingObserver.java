/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.general;

import cn.devezhao.bizz.privileges.impl.BizzPermission;
import cn.devezhao.commons.ThreadPool;
import com.rebuild.core.privileges.bizz.InternalPermission;
import com.rebuild.core.service.SafeObservable;
import com.rebuild.core.service.SafeObserver;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public abstract class OperatingObserver implements SafeObserver {

    protected OperatingObserver() {
        super();
    }

    @Override
    public void update(final SafeObservable o, final Object arg) {
        final OperatingContext ctx = (OperatingContext) arg;
        if (isAsync()) {
            ThreadPool.exec(() -> {
                try {
                    updateByAction(ctx);
                } catch (Exception ex) {
                    log.error("OperateContext : " + ctx, ex);
                }
            });
        } else {
            updateByAction(ctx);
        }
    }

    
    protected void updateByAction(final OperatingContext ctx) {
        if (ctx.getAction() == BizzPermission.CREATE) {
            onCreate(ctx);
        } else if (ctx.getAction() == BizzPermission.UPDATE) {
            onUpdate(ctx);
        } else if (ctx.getAction() == InternalPermission.DELETE_BEFORE) {
            onDeleteBefore(ctx);
        } else if (ctx.getAction() == BizzPermission.DELETE) {
            onDelete(ctx);
        } else if (ctx.getAction() == BizzPermission.ASSIGN) {
            onAssign(ctx);
        } else if (ctx.getAction() == BizzPermission.SHARE) {
            onShare(ctx);
        } else if (ctx.getAction() == InternalPermission.UNSHARE) {
            onUnshare(ctx);
        }
    }

    
    protected boolean isAsync() {
        return false;
    }

    

    
    protected void onCreate(final OperatingContext context) {
    }

    
    protected void onUpdate(final OperatingContext context) {
    }

    
    protected void onDelete(final OperatingContext context) {
    }

    
    protected void onDeleteBefore(final OperatingContext context) {
    }

    
    protected void onAssign(final OperatingContext context) {
    }

    
    protected void onShare(final OperatingContext context) {
    }

    
    protected void onUnshare(final OperatingContext context) {
    }
}
